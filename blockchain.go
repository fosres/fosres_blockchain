package main

import	(
	"fmt"
	"math"
)

type fieldelem struct	{
	
	num float64;

	prime float64;

}

type point struct	{

	a 	* fieldelem;

	b	* fieldelem;

	x	* fieldelem;

	y	* fieldelem;
} 

func is_valid_num(x * fieldelem)	bool	{
	
	return ( (x.num) > (x.prime) );
}

func is_equal(x * fieldelem, y * fieldelem)	bool	{
	return (x.num ) == (y.num);	

}

func arith(res int64,prime int64)		int64		{
	

	if res < 0	{

		return prime - ( res % prime ); 
	}

	return ( res % prime );

}

func point_on_curve(x * point)									bool	{
	
	if math.Pow( float64(x.y.num), float64(2) ) != ( math.Pow( float64(x.x.num),float64(3) ) + float64(x.a.num) * float64(x.x.num) + float64(x.b.num) )				{

		fmt.Printf("Error: The point (%f,%f) is not on curve\n",x.x.num,x.y.num);

		return false;
	}

	return true;

}

func points_eq(f * point,s * point)		bool {
	
	if f.x.num == s.x.num && f.y.num == s.y.num && f.a.num == s.a.num && f.b.num == s.b.num					{

		return true;
	}

	return false;
}

func addpoints(f * point, s * point)		* point					{

		if !point_on_curve(f)				{

			fmt.Printf("First point (%f,%f) is NOT on curve\n",f.x.num,f.y.num);

			return nil;
		
		}	else if	!point_on_curve(s)		{
			
			fmt.Printf("Second point (%f,%f) is NOT on curve\n",s.x.num,s.y.num);

			return nil;

		}	

		var s2 float64 = 0; 

		var x float64 = 0;

		var y float64 = 0;

		var res * point = nil;


		if !points_eq(f,s)			{
			

			 s2 = ( s.y.num - f.y.num ) / ( s.x.num - f.x.num );

			 x  = math.Pow(s2,2) - f.x.num - s.x.num;

			 y  = s2 * ( f.x.num - x ) - f.y.num;

		
		}	else				{
		
				s2 = ( 3.0 * math.Pow(f.x.num,2) + f.a.num ) / ( 2.0 * f.y.num );

				x = math.Pow(s2,2) - 2 * f.x.num; 

				y = s2 * ( f.x.num - x ) - f.y.num; 

		}
		
		var xt * fieldelem = &fieldelem{x,f.a.prime}; 
		
		var yt * fieldelem = &fieldelem{y,f.a.prime}; 

		res = &point{f.a,f.b,xt,yt};

		return res;
}

func multpoints(


func test_on_curve()				{

	var prime float64 = 223;
	
	var a * fieldelem = &fieldelem{0,prime};
	
	var b * fieldelem = &fieldelem{7,prime};

	var x * fieldelem = &fieldelem{0,prime};
	
	var y * fieldelem = &fieldelem{0,prime};

	var valid_pts [3][2]float64 = [3][2]float64{{192.0,105.0},{17.0,56.0},{1.0,193.0}};

	//var invalid_pts [2][2]float64 = [2][2]float64{{200,119},{42,99}};

	var p1 * point = nil;	

	var i uint64 = 0;

	for i < 3				{
		
		x.num = valid_pts[i][0];

		y.num = valid_pts[i][1];	

		p1 = &point{a,b,x,y};

		fmt.Printf("%b\n",point_on_curve(p1));

		i++;
	}
	
}

func main()					{

/*
	var a * fieldelem = &fieldelem{5,223};
	
	var b * fieldelem = &fieldelem{7,223};
	
	var x * fieldelem = &fieldelem{-1,223};
	
	var y * fieldelem = &fieldelem{-1,223};

	var f * point = &point{a,b,x,y};
	
	var s * point = &point{a,b,x,y};
	
	var res * point = addpoints(f,s);

	if res != nil			{

	fmt.Printf("Point addition of (%f,%f) + (%f,%f) == (%f,%f)\n",f.x.num,f.y.num,s.x.num,s.y.num,res.x.num,res.y.num);

	}
*/

	test_on_curve();
	
}

