#include <stdio.h>
#include <stdlib.h>

typedef struct fieldelem
{
	unsigned long long int num;

	unsigned long long int prime;

} fieldelem;

int is_valid_num(fieldelem * x)
{
	return ( (x->num) > (x->prime) );
}

int is_equal(fieldelem * x, fieldelem * y)
{
	return (x->num ) == (y->num);	

}
